import { useState, useEffect, useCallback } from "react";
import "./App.css";


function Title() {
  return <h1>Title</h1>
}

function Timer({text}) {
  return <h1>{text}</h1>
}

function Actions({startCount, resetCount}) {
  return (
    <div>
      <button onClick={startCount}>START</button>
      <button onClick={resetCount}>RESET</button>
    </div>
  )
}

function BoxWorkMins({ workMinsText, onWorkMinsTextChange }) {
  return (
    <input
      type="number"
      placeholder="Enter work mins..."
      value={workMinsText}
      onChange={(e) => onWorkMinsTextChange(e.target.value)}
    />
  )
}

function BoxWorkSecs({ workSecsText, onWorkSecsTextChange }) {
  return (
    <input
      type="number"
      placeholder="Enter work secs..."
      value={workSecsText}
      onChange={(e) => onWorkSecsTextChange(e.target.value)}
    />
  )
}

function BoxBreakMins({ breakMinsText, onBreakMinsTextChange }) {
  return (
    <input
      type="number"
      placeholder="Enter break mins..."
      value={breakMinsText}
      onChange={(e) => onBreakMinsTextChange(e.target.value)}
    />
  )
}

function BoxBreakSecs({ breakSecsText, onBreakSecsTextChange }) {
  return (
    <input
      type="number"
      placeholder="Enter break secs..."
      value={breakSecsText}
      onChange={(e) => onBreakSecsTextChange(e.target.value)}
    />
  )
}

function InputBoxes({arrayTexts, arrayHandlers}) {
  // iterate the arrays to build the components?
  return (
    <div>
      <div>
        <b>Work time</b>
        <BoxWorkMins workMinsText={arrayTexts[0]} onWorkMinsTextChange={arrayHandlers[0]} />
        <b>:</b>
        <BoxWorkSecs workSecsText={arrayTexts[1]} onWorkSecsTextChange={arrayHandlers[1]} />
      </div>
      <div>
        <b>Break time</b>
        <BoxBreakMins breakMinsText={arrayTexts[2]} onBreakMinsTextChange={arrayHandlers[2]} />
        <b>:</b>
        <BoxBreakSecs breakSecsText={arrayTexts[3]} onBreakSecsTextChange={arrayHandlers[3]} />
      </div>
    </div>
  )
}

function App() {

  const initialState = {
    work: {
      mins: "25",
      secs: "0"
    },
    break: {
      mins: "5",
      secs: "0"
    }
  }

  const [idInterval, setIdInterval] = useState(-1)
  const [workBreakTime, setWorkBreakTime] = useState(initialState)
  const [timerText, setTimerText] = useState(workBreakTime.work.mins+":"+workBreakTime.work.secs)
  const cbHandleStart = useCallback((time) => {
    handleStart();
  }, [handleStart]);

  useEffect(() => {
    async function startFetching() {
      cbHandleStart();
    }

    startFetching()
    return () => {
      setIdInterval(null)
    }
  }, [cbHandleStart])

  function handleWorkMinsText(text) {
    setWorkBreakTime({...workBreakTime, work: {...workBreakTime.work, mins: text}})
  }

  function handleWorkSecsText(text) {
    setWorkBreakTime({...workBreakTime, work: {...workBreakTime.work, secs: text}})
  }

  function handleBreakMinsText(text) {
    setWorkBreakTime({...workBreakTime, break: {...workBreakTime.break, mins: text}})
  }

  function handleBreakSecsText(text) {
    setWorkBreakTime({...workBreakTime, break: {...workBreakTime.break, secs: text}})
  }

  function decrementWorkSeconds() {
    setTimerText(current => {
      let [timerMins, timerSecs] = current.split(":")
      let timerMinsInt = parseInt(timerMins)
      let timerSecsInt = parseInt(timerSecs)

      if (timerSecsInt === 0) {
        timerSecsInt = 59
        timerMinsInt--
      } else {
        timerSecsInt--
      }

      return timerMinsInt + ":" + timerSecsInt
    })
  }

  function handleStart() {
    // check if an interval has already been set up
    // if ((idInterval === -1) || (idInterval === null)) {
    if (idInterval === (-1 || null)) {
      setIdInterval(setInterval(decrementWorkSeconds, 1000))
    }
  }

  function handleReset() {
    clearInterval(idInterval)
    setIdInterval(null)
    setTimerText(workBreakTime.work.mins + ":" + workBreakTime.work.secs)
  }

  return (
    <div className="App">
      <Title />
      <Timer text={timerText} />
      <Actions startCount={() => handleStart()} resetCount={() => handleReset()} />
      <InputBoxes
        arrayTexts={[workBreakTime.work.mins, workBreakTime.work.secs, workBreakTime.break.mins, workBreakTime.break.secs]}
        arrayHandlers={[handleWorkMinsText, handleWorkSecsText, handleBreakMinsText, handleBreakSecsText]}
      />
    </div>
  )
}

export default App
